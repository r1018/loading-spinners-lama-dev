<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/>

## Loading spinners in React

<img src="custom-loading-spinner.gif" alt="lifecycle methods" width="300" />
<img src="loading-dots-horizontal.gif" alt="lifecycle methods" width="300" />
<img src="loading-dots-vertical.gif" alt="lifecycle methods" width="300" />
<img src="loading-spinner.gif" alt="lifecycle methods" width="300" />
